package com.example.demo.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.User;

@Repository
public interface UserService extends JpaRepository<User, Long> {
	// todo

	String Q_GET_ALL_USERS = "select * from user u inner join contact c on u.contact_id=c.id";

	@Query(value = Q_GET_ALL_USERS,nativeQuery = true)
	public List<User> getAllUsers();

}